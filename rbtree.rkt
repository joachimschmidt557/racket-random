#lang racket
(define-struct node (value color left right))

(define (search node value)
  (cond
    [(= (node-value node) value) node]
    [(empty? node) empty]
    [(> (node-value node) value) (search (node-left node) value)]
    [else (search (node-right node) value)]))