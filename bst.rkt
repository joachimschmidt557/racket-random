#lang racket

(require rackunit)

(define-struct node (value left right))

(define (search node value)
  (cond
    [(empty? node) empty]
    [(= (node-value node) value) node]
    [(> (node-value node) value) (search (node-left node) value)]
    [else (search (node-right node) value)]))

(define (contains node value)
  (cond
    [(empty? node) #f]
    [(= (node-value node) value) #t]
    [(> (node-value node) value) (contains (node-left node) value)]
    [else (contains (node-right node) value)]))

(check-true (contains (make-node 3 empty empty) 3))
(check-false (contains (make-node 3 empty empty) 4))

(define (insert node value)
  (cond
    [(empty? node) (make-node value empty empty)]
    [(> (node-value node) value) (make-node (node-value node) (insert (node-left node) value) (node-right node))]
    [else (make-node (node-value node) (node-left node) (insert (node-right node) value))]))

(check-true (contains (insert empty 3) 3))
(check-false (contains (insert empty 3) 4))

(define (bst-from-list lst)
  (cond
    [(empty? lst) empty]
    [else (insert (bst-from-list (rest lst)) (first lst))]))

(check-true (contains (bst-from-list (list 3)) 3))
(check-true (contains (bst-from-list (list 3 4 2 8)) 2))
(check-false (contains (bst-from-list (list 3)) 4))
(check-false (contains (bst-from-list (list 3 7 2 1 5 6)) 4))

(define (rotate-right node)
  (if (empty? node) empty
      (let ([x node] [l (node-left node)] [r (node-right node)])
        (let ([a (if (empty? l) empty (node-left l))]
              [b (if (empty? l) empty (node-right l))]
              [c (if (empty? r) empty (node-left r))])
          (make-node (node-value l) a (make-node (node-value x) b r))))))

(define (height node)
  (cond
    [(empty? node) 0]
    [else (+ 1 (max (height (node-left node)) (height (node-right node))))]))

(define (preorder node)
  (cond
    [(empty? node) empty]
    [else (cons (node-value node) (append (preorder (node-left node)) (preorder (node-right node))))]))

(define (postorder node)
  (cond
    [(empty? node) empty]
    [else (append (postorder (node-left node)) (postorder (node-right node)) (list (node-value node)))]))

(define (inorder node)
  (cond
    [(empty? node) empty]
    [else (append (inorder (node-left node)) (cons (node-value node) (inorder (node-right node))))]))

(check-equal? (inorder (bst-from-list (list 5 2 4 3 7))) (list 2 3 4 5 7))

(define (avlcheck node)
  (cond
    [(empty? node) 0]
    [(= (avlcheck (node-left node)) -1) -1]
    [(= (avlcheck (node-right node)) -1) -1]
    [(< 1 (abs (- (avlcheck (node-left node)) (avlcheck (node-right node))))) -1]
    [else (+ 1 (max (avlcheck (node-left node)) (avlcheck (node-right node))))]))

(define (min-subtree node)
  (cond
    [(empty? node) empty]
    [(empty? (node-left node)) node]
    [else (min-subtree (node-left node))]))

(define (max-subtree node)
  (cond
    [(empty? node) empty]
    [(empty? (node-right node)) node]
    [else (max-subtree (node-right node))]))

(define (replacement node)
  (cond
    [(empty? node) empty]
    [(empty? (node-left node)) empty]
    [else (max-subtree (node-left node))]))

(define (sans-max-subtree node)
  (cond
    [(empty? node) empty]
    [(empty? (node-right node)) empty]
    [(empty? (node-right (node-right node))) (make-node (node-value node) (node-left node) empty)]
    [else (make-node (node-value node) (node-left node) (sans-max-subtree (node-right node)))]))

(define (delete node)
  (cond
    [(empty? node) empty]
    [(empty? (node-left node)) (node-right node)]
    [(empty? (node-right node)) (node-left node)]
    [else (make-node (node-value (replacement node)) (sans-max-subtree (node-left node)) (node-right node))]))

(check-equal? (delete empty) empty)
(check-equal? (delete (make-node 5 empty empty)) empty)
(check-equal? (make-node 5 empty empty) (make-node 5 empty empty))
(check-equal? (delete (make-node 6 (make-node 2 empty empty) empty)) (make-node 2 empty empty))