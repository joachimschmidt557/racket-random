;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-advanced-reader.ss" "lang")((modname pascal-line) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #t #t none #f () #f)))
(define (pascal-line n)
  (cond [(= n 0) (list 1)]
        [else (new-pascal-line (append (list 0) (pascal-line (- n 1)) (list 0)))]))

(define (new-pascal-line l)
  (cond [(< (length l) 2) empty]
        [else (cons (+ (first l) (second l)) (new-pascal-line (rest l)))]))