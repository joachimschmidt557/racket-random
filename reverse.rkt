#lang racket

(define (reverse lst)
  (cond
    [(empty? lst) empty]
    [else (append (reverse (rest lst)) (list (first lst)))]
    ))