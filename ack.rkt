;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-advanced-reader.ss" "lang")((modname ack) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #t #t none #f () #f)))
(define (ack n m)
  (cond [(= 0 n) (+ m 1)]
        [(= 0 m) (ack (- n 1) 1)]
        [else (ack (- n 1) (ack n (- m 1)))]))