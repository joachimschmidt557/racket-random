# racket-random

This repository contains some small snippets
of Racket code my brain has generated to this day.
The code is licensed under the Unlicense, so you may use
it for whatever you want, whenever you want.
