;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-advanced-reader.ss" "lang")((modname substr) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #t #t none #f () #f)))
(define (is-substring? s sub)
  (is-substring-expl? (explode s) (explode sub) (explode sub)))

(define (is-substring-expl? s sub backup)
  (cond [(empty? sub) #t]
        [(empty? s) #f]
        [(equal? (first s) (first sub))
         (is-substring-expl? (rest s) (rest sub) backup)]
        [(equal? (first s) (first backup))
         (is-substring-expl? (rest s) (rest backup) backup)]
        [else (is-substring-expl? (rest s) sub backup)]))